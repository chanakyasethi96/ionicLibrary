import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';



import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { UserPage } from '../pages/user/user';
import { AdminPage } from '../pages/admin/admin';
import {IssuedPage } from '../pages/user/issued/issued';
import {AvailablePage } from '../pages/user/available/available';
import {ProfilePage } from '../pages/user/profile/profile';
import { UserProvider } from '../providers/user/user';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BookdetailPage } from '../pages/user/available/bookdetail/bookdetail';



const firebaseAuth={
  apiKey: "AIzaSyCGaFvhlXIUTdywsufVMPjuogrhxzC6j4w",
  authDomain: "bookie-64d52.firebaseapp.com",
  databaseURL: "https://bookie-64d52.firebaseio.com",
  projectId: "bookie-64d52",
  storageBucket: "bookie-64d52.appspot.com",
  messagingSenderId: "141351128171"
}
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    UserPage,
    AdminPage,
    AvailablePage,
    IssuedPage,  
    ProfilePage,
    BookdetailPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAuth),
    AngularFireAuthModule
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    UserPage,
    AdminPage,
    AvailablePage,
    IssuedPage,
    ProfilePage,
    BookdetailPage
   ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider
  ]
})
export class AppModule {}
