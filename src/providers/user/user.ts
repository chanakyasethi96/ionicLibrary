import { Http,Response,Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  public user:any;
  public authToken:any;
  book:any;
  // userr:any;
  constructor(public http: Http) {
    console.log('Hello UserProvider Provider');
  }
  onIssueBook(bookid: any,userid: any){
    console.log('id',bookid)
    let headers=new Headers;
    headers.append('Content-Type','application/json');
    return this.http.put('http://localhost:3000/request/'+bookid+'/'+userid,{headers:headers})
    .map((res:Response) =>{
      return res.json();
    })
}
onReturnBook(book: any,userid:any){
    console.log(book._id);
    let headers=new Headers;
    headers.append('Content-Type','application/json');
    return this.http.put('http://localhost:3000/return/'+book._id+'/'+userid,{headers:headers})
    .map((res:Response) =>{
      return res.json();
    })
} 
IsssuedBook(){
    const user = this.user; 
    console.log("qqqqqqqq",user);   
    return this.http.get('http://localhost:3000/issuedbooks/'+user._id)
    .map((res:Response) =>{
      console.log(res);
      return res.json();
    })
}
getBooks(){
    return this.http.get('http://localhost:3000/allbooks')
            .map((res:Response) =>{
              return res.json();
            })
}
getBook(id: any){
    return this.http.get('http://localhost:3000/bookdetails/' + id)
            .map((res:Response) =>{
              this.book = res.json();
              console.log(res.json())
              return res.json();
            })
}
login(user){
  console.log('login== user',user);
  let headers=new Headers;
  headers.append('Content-Type','application/json');
  return this.http.post('http://localhost:3000/login',user,{headers:headers}) //JSON.stringify(user) = user
  .map((response:Response) =>{
  console.log('login response',response);  
  // if(response['success']){
      // console.log(response.json().user)
      localStorage.setItem('id_token',JSON.stringify(response.json().token));
      localStorage.setItem('user',JSON.stringify(response.json().user));     
       this.user=response.json().user;
       console.log('this user',response.json().user)
       this.authToken=response.json().token;
       return response.json();
  // } else{
      // return response.json();
  // }      
  });
}
registerUser(user){
  console.log("service",user)
  let headers=new Headers;
  headers.append('Content-Type','application/json');
  return this.http.post('http://localhost:3000/signup',user,{headers:headers})
  .map((res:Response) =>{
    return res.json();
  })
}
onLogOut()
{
  if(this.user)
  {
  console.log(this.user);
  this.http.get("http://localhost:3000/logout").subscribe((response:Response)=>{    
  });
  this.user=undefined;
  this.authToken=undefined;
  localStorage.clear();
}
}
}
