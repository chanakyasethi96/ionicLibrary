import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserProvider } from '../../providers/user/user'


import { LoginPage} from '../login/login';


/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  @ViewChild ('username')  username;
  @ViewChild ('password')  password;
  @ViewChild ('email')  email;

  constructor(public fire:AngularFireAuth,public navCtrl: NavController, public navParams: NavParams,private UserProvider:UserProvider ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  login(){
    this.navCtrl.push(LoginPage);
  }
  registerUser(){
    if(this.email.value){
      const newUser={
        name:this.username.value,        
        email:this.email.value,
        password:this.password.value,        
        // dob:value.doB,
        // gender:value.gender,
        // phone:value.pNumber,
        booksissued:[]
      }
           this.UserProvider.registerUser(newUser).
           subscribe(
             (user)=>{
               console.log("new user",newUser);
               this.navCtrl.push(LoginPage);         
             }
           );
    }  
  }
 
  
     }

