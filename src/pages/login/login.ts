import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterPage}from '../register/register';
import { AlertController } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserPage } from '../user/user';
import { AdminPage } from '../admin/admin';
import { UserProvider } from '../../providers/user/user'
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  @ViewChild ('username')  username;
  @ViewChild ('password')  password;

  constructor(public fire:AngularFireAuth,public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController,private UserProvider:UserProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  register(){
    this.navCtrl.push(RegisterPage);
  }
  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Coming Soon.......',
      buttons: ['Ok']
    });
    alert.present();
  }


  loginUser(){
    const user = {
        email:this.username.value,
        password:this.password.value
    }
    this.UserProvider.login(user).
    subscribe(
      (res)=>{
        if(res.success){
          console.log('===',res);
          if(this.username.value == 'admin@gmail.com'){
           this.navCtrl.push(AdminPage);
          } else {
           this.navCtrl.push(UserPage);
          }
        }
        else{
        //  this.router.navigate(['/'])
         console.log(res.msg) 
        }
  }
 )}

    
    
    
    
    }
