import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';
import { AdminPage } from '../admin/admin';

@NgModule({
  declarations: [
    LoginPage,
    AdminPage
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
  ],
  entryComponents:[
    AdminPage,
    LoginPage
  ]
})
export class LoginPageModule {}
